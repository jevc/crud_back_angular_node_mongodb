

const express = require('express');
const conectarDB = require('./config/db');
const cors = require('cors');
const path = require('path');

const port = process.env.PORT || 4000;

// creamos el servidor
const app = express();

conectarDB();
 
app.use(cors());
app.use(express.json());

app.use('/api/productos', require ('./routes/producto'));

// root route - serve static file
app.get('/', (req, res) => {
    return res.sendFile(path.join(__dirname, '/public/client.html'));
});

// root route - serve static file
app.get('/api/hello', (req, res) => {
    res.json({hello: 'goodbye'});
    res.end();
});

app.listen(port, () => {

    console.log("El servidor está corriendo perfectamente");

    /*    
    npm run dev
    */ 
});